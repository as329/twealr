class UsersController < ApplicationController

  # before_action :authenticate_user!

  def create
    @user = User.new(user_params)
    if @user.save
    flash[:notice] = "You have succesfully signed up!"
    redirect_to new_profile_path
    else
    render "new"
    end
  end

  def show
  end

  def index
  end

  #def self.from_omniauth(auth)
  #where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
   # user.email = auth.info.email
   # user.password = Devise.friendly_token[0,20]
    #user.name = auth.info.name   # assuming the user model has a name
    #user.image = auth.info.image # assuming the user model has an image
 # end
#end

#def self.new_with_session(params, session)
#    super.tap do |user|
#      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
#       user.email = data["email"] if user.email.blank?
#      end
#    end
#  end

 # after_create :build_profile

  def build_profile
    Profile.create(user: self) # Associations must be defined correctly for this syntax, avoids using ID's directly.
  end

end
