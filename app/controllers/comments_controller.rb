class CommentsController < SecuredController
  def create
    @answer = Answer.find(params[:answer_id])
    @comment = @answer.comments.create(comment_params)
    redirect_to answer_path(@answer)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end
