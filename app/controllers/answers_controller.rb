class AnswersController < SecuredController
  def new
  end

  def create
    @question = Question.find(params[:question_id])
    @answer = @question.answers.create(answer_params)

    if @answer.save
      redirect_to question_path(@question)
    else
      render 'new'
    end
  end

    def show
  @question = Question.find(params[:id])
  @answer = @question.answers.build
  end

  def index
  @answers = Answer.all
  end

  private
    def answer_params
      params.require(:answer).permit(:text)
    end

end
