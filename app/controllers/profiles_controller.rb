class ProfilesController < ApplicationController
before_action :get_profile, only: [:show, :edit, :update, :destroy]

  def new
    @user = User.find(params[:user_id])
    @profile = @user.profile.build
  end

  def create
    @user = User.find(params[:user_id])
    @profile = @user.profile.build(params[:profile])
    if @profile.save
      flash[:success] = "Your profile has been saved successfully."
    redirect_to new_user_proile_path(:user_id => @user)
    else
      flash[:notice] = 'Error. Something went wrong.'
      render :new
    end
  end

  def edit
  @profile = user.profile
  end

  def update
      if @profile.update(profile_params)
        redirect_to @profile, notice: 'The profile was successfully updated.'
        render :show, status: :ok, location: @profile
      else
       render :edit
      end
  end


  def destroy
    @profile.destroy
      redirect_to profile_url, notice: 'The profile was successfully deleted.'
  end

  def get_profile
    @user = User.find(params[:user_id])
    @profile = @user.profile.build
 end

  private

  def profile_params
    params.require(:profile).permit([:name, :photo])
  end

end
