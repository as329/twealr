class Question < ActiveRecord::Base
  has_many :answers
  has_many :comments
  validates :title, :presence => {:message => 'Title cannot be blank, Question not posted'},
                    length: {minimum: 5, maximum: 100}
  validates :text, :presence => {:message => 'Your question must contain some content, Question not posted'},
                    length: {minimum: 25, maximum: 500}
end
