class Answer < ActiveRecord::Base
  belongs_to :question
  has_many :comments
  validates :text, presence: true
end
