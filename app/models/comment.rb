class Comment < ActiveRecord::Base
  belongs_to :question
  belongs_to :answer
  validates :text, presence: true
end
