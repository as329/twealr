class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         #:omniauthable, :omniauth_providers => [:google_oauth2]
  has_many :questions
  has_many :answers
  has_many :comments
  has_one :profile

end
