class Profile < ActiveRecord::Base
  belongs_to :user
  validates :name, presence: true,
                    length: { minimum: 5, maximum: 140 }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

end
